( function( window ) {

'use strict';

var NGK = window.NGK;
var $ = window.jQuery;

var heroContainer;
var heroM;
var loadMoreButton;

NGK.index = function() {

  ( function() {

    $(window).load( function() {
      var hero = document.querySelector('#container');
      heroContainer = hero.querySelector('.hero-grid');
      heroM = new Masonry( heroContainer, {
        itemSelector: '.hero-item',
        columnWidth: '.grid-sizer'
      });

      getNewItems();
    });

  })();

  loadMoreButton = document.querySelector('#load-more-examples');
  eventie.bind( loadMoreButton, 'click', getNewItems );

};


var exampleOffset = 0;
var isLoading = false;

function getNewItems() {
  // don't load more stuff if already loading
  if ( isLoading ) {
    return;
  }

  isLoading = true;

  $.getJSON('http://www.json-generator.com/j/iFgi?indent=4'
    )
    .always( function() {
      isLoading = false;
    })
    .fail( getNewItemsFail )
    .done( getNewItemsSuccess );
}

function getNewItemsFail() {

}

function getNewItemsSuccess( data ) {
  // nothing more to load
  if ( !data || !data.length ) {
    loadMoreButton.style.display = 'none';

    return;
  }


  exampleOffset += data.length;
  var items = [];
  var fragment = document.createDocumentFragment();
  for ( var i=0, len = data.length; i < len; i++ ) {
    var item = makeNewItem( data[i] );
    items.push( item );
    fragment.appendChild( item );
  }

  imagesLoaded( fragment )
    .on( 'progress', function( imgLoad, image ) {
      var item = image.img.parentNode.parentNode;

      heroContainer.appendChild( item );
      heroM.appended( item );
    });

}

function makeNewItem( dataObj ) {
  var item = document.createElement('div');
  var link = document.createElement('a');
  var img = document.createElement('img');
  var title = document.createElement('p');

  item.className = 'hero-item has-example is-hidden';
  link.href = dataObj.url;
  img.src = dataObj.image.replace('/l.', '/m.');
  
  title.className = 'example-title';
  title.textContent = dataObj.title;

  link.appendChild( img );
  link.appendChild( title );
  item.appendChild( link );
  return item;
}

})( window );