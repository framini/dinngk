( function( window ) {

'use strict';

var NGK = window.NGK = {};

NGK.pages = {};
var notifElem;

docReady( function() {

  notifElem = document.querySelector('#notification');

  // get name of page
  var pageAttr = document.body.getAttribute('data-page');

  // trigger controller if there
  if ( pageAttr && typeof NGK[ pageAttr ] === 'function' ) {
    NGK[ pageAttr ]();
  }
});

// -------------------------- helpers -------------------------- //

NGK.getSomeItemElements = function() {
  var fragment = document.createDocumentFragment();
  var items = [];
  for ( var i=0; i < 3; i++ ) {
    var item = document.createElement('div');
    var wRand = Math.random();
    var widthClass = wRand > 0.85 ? 'w4' :
      wRand > 0.7 ? 'w2' : '';
    var hRand = Math.random();
    var heightClass = hRand > 0.85 ? 'h4' :
      hRand > 0.7 ? 'h2' : '';
    item.className = 'item ' + widthClass + ' ' + heightClass;
    fragment.appendChild( item );
    items.push( item );
  }
};

// ----- text helper ----- //

var docElem = document.documentElement;
var textSetter = docElem.textContent !== undefined ? 'textContent' : 'innerText';

function setText( elem, value ) {
  elem[ textSetter ] = value;
}

})( window );
