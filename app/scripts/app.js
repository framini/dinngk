'use strict';

$(function() {
    var $nkgw = $('body');
    
    var outerPane = $nkgw,
    didScroll = false;

    $(window).scroll(function() {
        didScroll = true;
    });
     
    setInterval(function() {
        if ( didScroll ) {
            didScroll = false;

            if( $(window).scrollTop() === 0 ) {
              $('.stuck').removeClass('stuck');
              $.waypoints('refresh');
            }
        }
    }, 250);

    $('#page-header').waypoint('sticky');
})
