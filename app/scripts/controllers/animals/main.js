'use strict';



angular.module('NatGeoKidsAppProfile')
  .directive("masonry", function () {
	    var NGREPEAT_SOURCE_RE = '<!-- ngRepeat: ((.*) in ((.*?)( track by (.*))?)) -->';
	    return {
	        compile: function(element, attrs) {
	            // auto add animation to brick element
	            var animation = attrs.ngAnimate || "'masonry'";
	            var $brick = element.children();
	            $brick.attr("ng-animate", animation);
	            
	            // generate item selector (exclude leaving items)
	            var type = $brick.prop('tagName');
	            var itemSelector = type+":not([class$='-leave-active'])";
	            
	            return function (scope, element, attrs) {
	                var options = angular.extend({
	                    itemSelector: itemSelector
	                }, scope.$eval(attrs.masonry));
	                
	                // try to infer model from ngRepeat
	                if (!options.model) { 
	                    var ngRepeatMatch = element.html().match(NGREPEAT_SOURCE_RE);
	                    if (ngRepeatMatch) {
	                        options.model = ngRepeatMatch[4];
	                    }
	                }
	                
	                // initial animation
	                element.addClass('masonry');
	                
	                // Wait inside directives to render
	                setTimeout(function () {

	                    element.freetile({
	                    	animate : true,
	                    	callback: function() {
	                    		scope.loadComplete = true;
	                    	}
			            });
	                    
	                    
	                    if (options.model) {
	                        scope.$apply(function() {
	                            scope.$watchCollection(options.model, function (_new, _old) {
	                                if(_new == _old) return;           
	                            	element.freetile({
	                            		callback: function() {
	                            			$(window).resize();
	                            			console.log("finalizo desde la segunda inicializacion")
	                            		}
	                            	});
	                            });
	                        });
	                    }
	                });
	            };
	        }
	    };
   })

  .controller('MainCtrl', function ($scope, $http, $q, $timeout) {

    $scope.isViewLoading = false;

	$scope.$on('$routeChangeStart', function() {
	  $scope.isViewLoading = true;
	});

	$scope.$on('$routeChangeSuccess', function() {	
	  $scope.isViewLoading = false;
	});

	$scope.items = $http.get('http://www.json-generator.com/j/cgRWpopGzS?indent=4')
    .then(function (response) {

    	var uniqueTypes = _.uniq(_.pluck(response.data, 'type'));
    
		$scope.animalFilters = _.map(uniqueTypes, function(animal){
		    return { name : $.trim(animal), status : false};
		});

        return response.data;
    });

    $scope.loadComplete = false;

    $scope.search = function (row) {
        return !!((row.name.indexOf($scope.query || '') !== -1 || row.type.indexOf($scope.query || '') !== -1));
    };

    
    /**/

    $scope.animalFilterCheckboxes = {};
    $scope.brandCheckboxes = {};

    function getChecked(obj){

        var checked = [];
        for(var key in obj) if(obj[key]) checked.push(key);
        return checked;
    }

    $scope.searchFilter = function(row){
        var mercChecked = getChecked($scope.animalFilterCheckboxes);
        if(mercChecked.length == 0)
            return true;
        else{
            if($scope.animalFilterCheckboxes[row.type]) {
                return true;
            } else{
                return false;
            }
        }
    };

    $scope.onlyTrending = function(row){

        if(row.trending) {
            return true;
        } else {
        	return false;
        }
    };


		/*$scope.animalFilters = [
			{name: "Cat"},
			{name: "Lion"}
		]*/
    /**/

  });

