'use strict';

angular.module('NatGeoKidsApp', ['infinite-scroll', 'ballMenu', 'dinWidth'])
    .controller('MainCtrl', function($scope, Reddit) {
        $scope.reddit = new Reddit();
    });

angular.module('NatGeoKidsApp')
    .directive('dragable', function() {
        return {
            controller: function($scope) {
                $scope.menu = {
                    visible: false,
                    cerrar: true
                };

                $scope.toggleMenu = function() {

                    if($scope.menu.cerrar) {
                        $scope.menu.visible = !$scope.menu.visible;
                    } else {
                        $scope.menu.cerrar = true;
                    }
                    
                    $scope.$apply();
                }

                $scope.cerrarMenu = function(estado) {
                    $scope.menu.cerrar = estado;
                    
                    $scope.$apply();
                }
            },
            link: function(scope, iElement, iAttrs, controller) {

                iElement.addClass('loaded');

                $('.main-menu-icon').bind('click', function() {
                    scope.toggleMenu();
                });

                Draggable.create(".main-menu", {
                    type: "x,y",
                    edgeResistance: 0.65,
                    bounds: window,
                    throwProps: true,
                    onDrag: function() {
                        scope.cerrarMenu(false);
                    }
                });
            }
        }
    })


angular.module('NatGeoKidsApp')
.factory('Reddit', function($http) {
  var Reddit = function() {
    this.items = [];
    this.busy = false;
    this.after = '';
  };

  var isLoading = false;

  Reddit.prototype.nextPage = function() {

    if (isLoading) return;
    isLoading = true;
    this.bussy = true;

    // Example JSON
    var images = {
        0 : {
            "padding" : "",
            "text" : true,
            "color" : "orange",
            "img" : true
        },
        1 : {
            "padding" : "no-padding",
            "text" : false,
            "color" : "light-orange",
            "img" : true
        },
        2 : {
            "padding" : "",
            "text" : true,
            "color" : "red",
            "img" : true
        },
        3 : {
            "padding" : "",
            "text" : true,
            "color" : "blue",
            "img" : true
        },
        4 : {
            "padding" : "",
            "text" : true,
            "color" : "cyan",
            "img" : true
        },
        5 : {
            "padding" : "no-padding",
            "text" : false,
            "color" : "light-orange",
            "img" : true
        },
        6 : {
            "padding" : "no-padding",
            "text" : false,
            "color" : "green",
            "img" : true
        },
        7 : {
            "padding" : "",
            "text" : true,
            "color" : "purple",
            "img" : true
        },
        8 : {
            "padding" : "",
            "text" : true,
            "color" : "green",
            "img" : true
        },
        9 : {
            "padding" : "",
            "text" : true,
            "color" : "blue",
            "img" : true
        },
        10 : {
            "padding" : "",
            "text" : true,
            "color" : "cyan",
            "img" : true
        },
        11 : {
            "padding" : "",
            "text" : true,
            "color" : "purple",
            "img" : false
        },
        12 : {
            "padding" : "",
            "text" : true,
            "color" : "green",
            "img" : false
        }
    };

    var url = "http://api.reddit.com/hot?after=" + this.after + "&limit=12" + "&jsonp=JSON_CALLBACK";

    function makeNewItem(dataObj) {
        var item = document.createElement('div');
        var link = document.createElement('a');
        //var img = document.createElement('img');
        var title = document.createElement('p');
        var randomImg = Math.round(getRandomNumber(1, 12));
        
        var randomNbr = getRandomNumber(0,1);
        var isPremium = false;
        var premiumClass = 'premium';

        if(randomNbr < 0.4) {
            isPremium = true;
        }

        item.className = isPremium ?  
                            'box-content din ' + images[randomImg].padding + " " + images[randomImg].color + " " + premiumClass 
                        : 
                            'box-content din ' + images[randomImg].padding + " " + images[randomImg].color;


        var img = document.createElement('img');
        var size = Math.random() * 3 + 1;
        var width = Math.random() * 110 + 100;
        width = Math.round( width * size );
        var height = Math.round( 140 * size );
        var rando = Math.ceil( Math.random() * 1000 );

        link.href="#";
        link.className = "premium no-access";

        img.src = 'http://lorempixel.com/' + width + '/' + height + '/' + 'animals/' + '?' + rando;


        if( images[randomImg].img ) {
            /*if( isPremium ) {
                link.appendChild(img);
                item.appendChild(link);
            } else {*/
                item.appendChild(img);
           //}
        }

        if(images[randomImg].text) {
            title.textContent = dataObj.data.title;
            item.appendChild(title);
        }
        
        return item;
    }

    function getRandomNumber(min, max) {
      return Math.random() * (max - min) + min;
    }

    $http.jsonp(url).success(function(data) {
      var items = data.data.children;
      var that = this;
   
      var fragment = document.createDocumentFragment();

      for (var i = 0, len = items.length; i < len; i++) {
            var item = makeNewItem(items[i]);

            fragment.appendChild(item);
      }

      imagesLoaded( fragment )
        .on( 'progress', function( imgLoad, image ) {
          var item = $(image.img).closest('.box');

          var contItem = document.createElement('div');
          contItem.className = "box tile columns bounceIn animated slow";
          contItem.appendChild(image.img.parentNode)

          $('#container').freetile({
                contentToAppend: $(contItem),
                callback: function() {
                    $(window).resize();
                }
            });

          /*$(contItem).bind('click', function() {
            console.log("DASDASDASDASDASDASDASDASDASDSAD");
          })*/

          $(contItem).on('click', function () {
                $.fn.custombox( this, {
                    effect: 'sidefall',
                    url: "views/premium/base.html",
                    overlayColor : "fff",
                    overlayOpacity : 0.9
                });
                return false;
          });

        });
    }.bind(this))
     .then( function() {
       isLoading = false;
       //this.bussy = false;
     });
  };

  return Reddit;
});

$(function() {
    $('#container').freetile({
        animate: true,
        elementDelay: 3
    })

    $(window).resize();

    $('.premium').on('click', function () {
            $.fn.custombox( this, {
                effect: 'sidefall',
                url: "views/premium/base.html",
                overlayColor : "fff",
                overlayOpacity : 0.9
            });
            return false;
      });
});

