'use strict';

angular.module('NatGeoKidsAppProfile')
  .controller('MainCtrl', function ($scope, $http, $q, $timeout) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.isViewLoading = false;

	$scope.$on('$routeChangeStart', function() {
	  $scope.isViewLoading = true;
	});

	$scope.$on('$routeChangeSuccess', function() {	
	  $scope.isViewLoading = false;
	});

	$scope.profile = {
		percentage : {
			intelligence : 60,
			strength: 80
		},
		info : {
			percentage : {
				intelligence : 60,
				strength: 80,
				speed: 50,
				cuteness: 40
			}
		},
		players: {
			player1 : {
				clase : 'player-1'
			},
			player2 : {
				clase : 'player-2'
			}
		}
	}
  });