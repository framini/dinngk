'use strict';

angular.module('NatGeoKidsAppProfile')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/profile/main.html',
        controller: 'MainCtrl'
      })
      .when('/videos', {
        templateUrl: 'views/profile/videos.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
