angular.module("dinWidth",[])
  .directive("dinWidth", ["$window", function($window){
    return {
      restrict : "AC",
            controller: function($scope) {        
            },
            link: function(scope, iElement, iAttrs, controller) {
                var sz;
                var w = angular.element($window);
                $(iElement).addClass('overflow-hidden');
                if(iAttrs.dinWidth) {
                    sz =  $($window).width();
                } else {
                    sz = $($window).width() - 30;
                }

                scope.$on('$routeChangeSuccess', function() {
                    $(iElement).css('max-width', sz)
                });

                w.bind('resize', function() {
                    $(iElement).css('max-width', sz)
                })

                
            }
    };
}]);