angular.module("ballMenu",['ngAnimate'])
  .directive("ballMenu", ["$window", function($window){
    return {
      restrict : "AC",
            controller: function($scope) {
                $scope.menu = {
                    visible: false,
                    cerrar: true,
                    orientation: "menu-orientation-right"
                };

                var xMiddle = $(window).width() / 2;
                var yMiddle = $(window).height() / 2;

                $scope.toggleMenu = function(element) {
                    console.log($scope.dragObj);

                    if(!$scope.dragObj.isDragging && !$scope.menu.visible) {
                        $scope.menu.visible = !$scope.menu.visible;
                        $scope.menu.cerrar = !$scope.menu.cerrar;

                        // Facebook behavior mobile
                        if(Modernizr.mq("only screen and (max-width: 480px)")) {
                            console.log("CAMBIO")
                            $('.modal-bg').show();
                            if($scope.dragObj.pointerX > xMiddle) {
                                $scope.dragObj.moveTo(element, $scope.dragObj.maxX, 0);
                            } else {
                                $scope.dragObj.moveTo(element, $scope.dragObj.minX, 0);
                            }
                        }

                    } else {
                        console.log("ENTRO")
                        $scope.menu.cerrar = true;
                        $scope.menu.visible = false;
                        $('.modal-bg').hide();
                    }
                    
                    $scope.$apply();
                }

                $scope.setOrientation = function() {
                    if($scope.dragObj.pointerX > xMiddle) {
                        $scope.menu.orientation = "menu-orientation-left";
                    } else {
                        $scope.menu.orientation = "menu-orientation-right";
                    }

                    $scope.$apply();
                }

                $scope.cerrarMenu = function(estado) {
                    $scope.menu.cerrar = estado;
                    
                    $scope.$apply();
                }

                $scope.setDragObj = function(elem) {
                    $scope.dragObj = elem;
                    
                    $scope.$apply();
                }
            },
            link: function(scope, iElement, iAttrs, controller) {

                iElement.addClass('loaded');

                $(iElement).parent().prepend('<div class="modal-bg hide"></div>');

                var gridWidth = $(window).width() / 2;
                var gridHeight = $(window).height() / 2;
                var d = Draggable.create(".main-menu", {
                    type: "x,y",
                    edgeResistance: 0.65,
                    bounds: window,
                    throwProps: true,
                    onDrag: function() {
                        scope.cerrarMenu(false);
                        scope.setDragObj(this);
                        scope.setOrientation();
                        console.log('onDrag')
                    },
                    onThrowComplete : function() {
                        if(Modernizr.mq("only screen and (max-width: 480px)")) {
                            if( this.pointerX > gridWidth ) {
                                this.moveTo(this.target, this.maxX, this.y);
                            } else {
                                this.moveTo(this.target, this.minX, this.y);
                            }
                        }
                    },
                    maxDuration : .8,
                    onClick : function() {
                        console.log("Entro a onClick");
                        scope.setDragObj(this);
                        scope.toggleMenu(this.target);
                    },
                    trigger : $('.main-menu-icon')
                });

                //TweenLite.set(d[0].target, {x: "350px"});
            }
    };
}]);