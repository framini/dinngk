angular.module("dinHeight",[])
  .directive("dinHeight", ["$window", function($window){
    return {
      restrict : "AC",
            controller: function($scope) {        
            },
            link: function(scope, iElement, iAttrs, controller) {
                scope.$on('$routeChangeSuccess', function() {
                    $(iElement).css('min-height', $('.profile-content-animation').height())
                });
            }
    };
}]);